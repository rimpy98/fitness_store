# Generated by Django 2.1.7 on 2019-05-02 18:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0007_category1_product'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='category1',
            name='sub_cat',
        ),
        migrations.RemoveField(
            model_name='product',
            name='product_category',
        ),
        migrations.AlterField(
            model_name='product',
            name='photo',
            field=models.ImageField(blank=True, upload_to='media/%Y/%m/%d'),
        ),
        migrations.DeleteModel(
            name='category',
        ),
        migrations.DeleteModel(
            name='category1',
        ),
    ]
