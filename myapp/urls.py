from django.urls import path
from myapp import views
app_name='myapp'
urlpatterns=[
    path('',views.index,name='index'),
    path('about/',views.about,name='about'),
    path('contact/',views.contactForm,name='contact'),
    path('signup/',views.signup,name='signup'),
    path('checkuser/',views.checkuser,name='checkuser'),
    path('product/',views.productView,name='product'),
    path('dashboard/',views.dashboard,name='dashboard'),
    path('logout/',views.uslogout,name='logout'),



]
