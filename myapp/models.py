from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class contact(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(max_length=200)
    message = models.TextField()

    def __str__(self):
        return self.name


class register(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    contact = models.IntegerField()
    address = models.TextField()
    registered_on = models.DateTimeField(auto_now_add = True)
    changed_on = models.DateTimeField(auto_now = True)

    def __str__(self):
        return self.user.first_name+' '+self.user.last_name

class category(models.Model):
    category_name = models.CharField(max_length=250)
    sub_cat = models.ForeignKey('self',on_delete=models.CASCADE,null=True,blank=True)
    create_date = models.DateTimeField(auto_now_add = True)
    updated_on = models.DateTimeField(auto_now = True)

    def __str__(self):
        return self.category_name            
    


class product(models.Model):
    product_name = models.CharField(max_length=300)
    product_category = models.ForeignKey(category,on_delete=models.CASCADE, null=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    sale_price = models.DecimalField(max_digits=10, decimal_places=2)
    photo = models.ImageField(upload_to='media/%Y/%m/%d',blank=True)
    description = models.TextField()
    create_date = models.DateTimeField(auto_now_add = True)
    updated_on = models.DateTimeField(auto_now = True)

    def __str__(self):
        return self.product_name            
      

