from django.shortcuts import render
from myapp.models import contact
from myapp.models import register,category,product
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import check_password
import datetime


# Create your views here.
all_cat = category.objects.all().order_by('-id')
all_pro=product.objects.all().order_by('-id')
def index(request):
    return render(request,'index.html')

def about(request):
    return render(request,'about.html')    

def contactForm(request):
    if request.method=='POST':
        print(request.POST)
        n=request.POST['name']    
        e=request.POST['email']    
        m=request.POST['message']    

        data=contact(name=n,email=e,message=m)
        data.save()
        context={'status':'Dear {} thnx for your feedback'.format(n)}
        return render(request,'contact.html',context)

    return render(request,'contact.html')

def signup(request):
    if 'username' in request.COOKIES:
        un=request.COOKIES['username']
        user_obj=User.objects.get(username=un)
        login(request,user_obj)
        return HttpResponseRedirect('/myapp/product')

    if request.method == 'POST':
        if 'signupbtn' in request.POST:
            first = request.POST['first_name']
            last = request.POST['last_name']
            username = request.POST['username']
            password = request.POST['password']
            email = request.POST['email']
            addr = request.POST['address']
            con = request.POST['contact']

            data = User.objects.create_user(username,email,password)
            data.first_name = first
            data.last_name = last
            data.save()

            data1 = register(user=data,contact=con,address=addr)
            data1.save()
            return render(request,'signup.html',{'status':'Registered Successfully!!'})

        if 'loginbtn' in request.POST:
            un = request.POST['username']
            pwd = request.POST['password']
            
            user = authenticate(username=un,password=pwd)
            if user:
                print('KJHGFD')
                if user.is_active:
                    login(request,user)
                    response=HttpResponseRedirect('/myapp/dashboard')
                    if 'rememberme' in request.POST: 
                        response.set_cookie('username',un)
                        response.set_cookie('userid',user.id)
                        response.set_cookie('logintime',datetime.datetime.now())
                        print('COOKIE CREATED')
                    
                    
                    return response
            else:
                return render(request,'signup.html',{'status':'Invalid login details'})
    
    return render(request,'signup.html')

    

@login_required
def uslogout(request):
    logout(request)
    response = HttpResponseRedirect('/')
    response.delete_cookie('username')
    response.delete_cookie('logintime')
    response.delete_cookie('id')
    return response       

def checkuser(request):
    if request.method=='GET':
        un = request.GET['usern']
        ch = User.objects.filter(username=un)
        if len(ch)>=1:
            return HttpResponse('A User with this name alreay exists!!')
        else:
            return HttpResponse('Username validation success!!')


def productView(request):
    if 'cat' in request.GET:
        catg = request.GET['cat']
        products = product.objects.filter(product_category__category_name=catg)
        return render(request,'product.html',{'all':products,'total':len(products)})   
    return render(request,'product.html',{'all':all_pro,'total':len(all_pro)})    
    

def dashboard(request):
    return render(request,'dashboard.html',{'all':all_cat})    


