# Generated by Django 2.1.7 on 2019-04-17 15:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0003_register'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='register',
            name='user',
        ),
        migrations.DeleteModel(
            name='register',
        ),
    ]
