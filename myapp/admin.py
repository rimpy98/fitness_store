from django.contrib import admin
from myapp.models import contact
from myapp.models import register
from myapp.models import category,product

admin.site.register(contact)
admin.site.register(register)
admin.site.register(category)
admin.site.register(product)
